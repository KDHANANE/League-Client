package league.client.league.client;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import league.client.league.client.entity.League;
import league.client.league.client.entity.Player;
import league.client.league.client.entity.Team;

public class App {

	static Client client = ClientBuilder.newClient();

	public static void main(String[] args) {

		League championsLeague = new League("Champions League", "football");
		Team psg = new Team("psg", "1ere division", championsLeague, "city1");
		Player kurzawa = new Player("kurzawa", "hhhhhh", 6, psg, "actif", (double) 10000);
		//createPlayer(kurzawa);
		
		League nfl = new League("NAF NAF LEAGUE", "football");
		League league1 = new League("league1", "soccer");
		League nba = new League("nba", "basketball");

		List<Team> teams = new ArrayList<Team>();
		List<League> leagues = new ArrayList<League>(Arrays.asList(nfl, league1, nba));
		List<Player> players = new ArrayList<Player>();

		int nbrEquipeParLeague;
		int nbrEquipes = 10;
		nbrEquipeParLeague = nbrEquipes / 3;
		int debut = 0;
		int fin = nbrEquipeParLeague;
		Random random = new Random();

		for (League league : leagues) {

			for (int i = debut; i < fin; i++) {
				Team team = new Team("team" + String.valueOf(i), "1ere division", league,
						"Oakland" + String.valueOf(i));

				teams.add(team);

				for (int j = 0; j < 10; j++) {
					String status = j % 2 == 0 ? "Actif" : "Inactif";
					Player player = new Player("player" + String.valueOf(j), "Mickael", random.nextInt(), team, status,
							random.doubles(6).sum());
					players.add(player);
					
					createPlayer(player);
				
						}
								
			}
		
			Team team1 = new Team("team sans league", "League1", null, "Oakland");
			teams.add(team1);
			Player player1 = new Player("player", "Mickael", random.nextInt(), null, "Actif", random.doubles(6).sum());
			players.add(player1);

			debut = fin;
			fin += nbrEquipeParLeague;
			
		}
		
		//Player player = new Player("kljdlksdj", "nnn", 5, null, "", 5454D);
		
		System.out.println("Liste cr��e....!!!!");
	}
	
	
	

	private static Player createPlayer(Player player) {

		return client.target("http://10.0.0.68:8080/league/webapi/player")
				.queryParam("name", player.getName())
				.queryParam("firstName", player.getFirstName())
				.queryParam("jerseyNumber", player.getJerseyNumber())
				.queryParam("status", player.getStatus())
				.queryParam("salary", player.getSalary())
				.request(MediaType.APPLICATION_JSON).get(Player.class);
		}
	
	private static List<Player> findPlayers(List<Player> players) {
			return client.target("http://10.0.0.68:8080/league/webapi/player/all")        
					.request(MediaType.APPLICATION_JSON)        
					.get(new GenericType<List<Player>>() {});
			}
	
	
	
}

