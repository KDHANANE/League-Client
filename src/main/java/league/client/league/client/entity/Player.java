package league.client.league.client.entity;

import java.util.Date;


import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement
public class Player extends Identifier {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1248851851826750062L;

	private String firstName;
	private int jerseyNumber;
	private String status;
	private Double salary;

	public Player() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Player(String name, String firstName, int jerseyNumber, Team team, String status, Double salary) {
		super(name);
		this.firstName = firstName;
		this.jerseyNumber = jerseyNumber;
		this.team = team;
		this.status = status;
		this.salary = salary;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	

	public Double getSalary() {
		return salary;
	}

	public void setSalary(Double salary) {
		this.salary = salary;
	}

	
	private Team team;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public int getJerseyNumber() {
		return jerseyNumber;
	}

	public void setJerseyNumber(int jerseyNumber) {
		this.jerseyNumber = jerseyNumber;
	}

	public Team getTeam() {
		return team;
	}

	public void setTeam(Team team) {
		this.team = team;
	}

	@Override
	public String toString() {
		return "Player [firstName=" + firstName + ", jerseyNumber=" + jerseyNumber + ", status=" + status
				+ ", salary=" + salary + ", team=" + team + "]";
	}

	
	

	

	
	
	

}
